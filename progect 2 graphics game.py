from graphics import *
makeGraphicsWindow(1400, 800)
 
############################################################
# this function is called once to initialize your new world
 
def startWorld(world):
    
    
    world.youwin = loadImage("youwin.png", scale = 1.5) 
    world.youwinX = 700
    world.youwinY = 400
    world.youlose = loadImage("youlose.png", scale = 1.5)
    world.youloseX = 700
    world.youloseY = 400
    world.cant =True 
    world.rodanthavecheese0 = False 
    world.rodanthavecheese1 = False
    world.rodanthavecheese2 = False
    world.rodanthavecheese3 = False
    world.rodanthavecheese4 = False
    world.rodanthavecheese5 = False
    World.rodanthaveyarn= False 
    world.rodanthavelife = True
    world.rodant = loadImage("rodant.png", scale =.5)
    world.rodantX= 100
    world.rodantY= 300
    
    world.lazers = loadImage("lazers.png.gif", scale =.25)
    world.shooting = False 
    world.lazerspeed =0 
    world.lazersX = 100
    world.lazersY = 300
    onKeyPress(lazers, "space")
    
    world.cheese0 = loadImage("cheese0.png", scale = .5)
    world.cheese0X= 150
    world.cheese0Y= 200
    
    world.cheese1 = loadImage("cheese1.png", scale =.5)
    world.cheese1X= 150
    world.cheese1Y= 600
    
    world.cheese2 = loadImage("cheese2.png", scale =.5)
    world.cheese2X= 550
    world.cheese2Y= 200 
    
    world.cheese3 = loadImage("cheese3.png", scale =.5)
    world.cheese3X= 550
    world.cheese3Y= 600
   
    world.cheese4 = loadImage("cheese4.png", scale = .5)
    world.cheese4X= 950
    world.cheese4Y= 200
    
    world.cheese5 = loadImage("cheese5.png", scale = .5)
    world.cheese5X= 950
    world.cheese5Y= 600
    
    world.cat = loadImage("cat.png", scale = .5)
    world.catX= 1300
    world.catY= 400  
    
    world.yarn= loadImage("yarn.png", scale =.5 )
    world.yarnX= 1350
    world.yarnY= 400   
    
    world.trap0= loadImage("trap0.png", scale =.5)
    world.trap0X= 150
    world.trap0Y= 600 
    
    world.trap1= loadImage("trap1.png", scale = .5)
    world.trap1X= 550
    world.trap1Y= 600 
    
    world.trap2= loadImage("trap2.png", scale = .5)
    world.trap2X= 950
    world.trap2Y= 600
    
    world.traplife0=True
    world.traplife1=True
    world.traplife2=True     
  
   
     
def lazers(world):
    world.shooting= True 
    world.lazerspeed =10
    
    
    

############################################################    
 
############################################################
# this function is called every frame to update your world
 
def updateWorld(world):
    if world.rodantX > 1500:
        world.rodantX = -40  
    world.catY = world.catY +5
    if world.catY > 800:
        world.catY = 0
    
    if world.traplife0 == True:  
        world.trap0Y =world.trap0Y +4
        if world.trap0Y > 800:
            world.trap0Y = 0
    if world.traplife1 ==True:
        world.trap1Y =world.trap1Y +7
        if world.trap1Y > 800:
            world.trap1Y = 0
    if world.traplife2 == True:  
        world.trap2Y =world.trap2Y +6
        if world.trap2Y > 800:
            world.trap2Y = 0      
    
    
    if world.shooting==True: 
        world.lazersX += world.lazerspeed 
        speed = 5
        if isKeyPressed("Down"):
            world.rodantY  += speed
            
        if isKeyPressed("Up"):
            world.rodantY -= speed 
            
        if isKeyPressed("Right"):
            world.rodantX += speed
            
        if isKeyPressed("Left"):
            world.rodantX -= speed
            
        if world.lazersX <= world.trap0X + 75 and world.lazersY <= world.trap0Y + 75 and world.lazersX >= world.trap0X - 75 and world.lazersY >= world.trap0Y - 75:
                    world.traplife0 = False 
        if world.lazersX <= world.trap1X + 75 and world.lazersY <= world.trap1Y + 75 and world.lazersX >= world.trap1X - 75 and world.lazersY >= world.trap1Y - 75:
                    world.traplife1 = False 
        if world.lazersX <= world.trap2X + 75 and world.lazersY <= world.trap2Y + 75 and world.lazersX >= world.trap2X - 75 and world.lazersY >= world.trap2Y - 75:
                    world.traplife2 = False        
                      
    
    
    
    if world.shooting == False:
        speed = 5
        if isKeyPressed("Down"):
            world.rodantY  += speed
            world.lazersY += speed
                    
        if isKeyPressed("Up"):
            world.rodantY -= speed 
            world.lazersY -= speed
                
        if isKeyPressed("Right"):
            world.rodantX += speed
            world.lazersX += speed        
                
        if isKeyPressed("Left"):
            world.rodantX -= speed        
            world.lazersX -= speed
        
    
    if world.rodantX > 125 and world.rodantX <200 and world.rodantY < 260 and world.rodantY > 22:
        world.rodanthavecheese0 = True  
    if world.rodantX > 115 and world.rodantX < 225 and world.rodantY < 625 and world.rodantY > 525:
        world.rodanthavecheese1 = True 
    if world.rodantX > 500 and world.rodantX < 600 and world.rodantY < 275 and world.rodantY > 100:
        world.rodanthavecheese2 = True
    if world.rodantX > 500 and world.rodantX < 600 and world.rodantY < 625 and world.rodantY > 525:
        world.rodanthavecheese3 = True
    if world.rodantX > 850 and world.rodantX < 1000 and world.rodantY < 275 and world.rodantY > 100:
        world.rodanthavecheese4 = True 
    if world.rodantX > 850 and world.rodantX < 1000 and world.rodantY < 625 and world.rodantY > 525:
        world.rodanthavecheese5 = True
    if world.rodantX > 1300 and world.rodantX <1400 and world.rodantY <450 and world.rodantY > 350:
        world.rodanthaveyarn= True
    if world.traplife0==True:   
        if world.trap0X <= world.rodantX + 75 and world.trap0Y <= world.rodantY + 75 and world.trap0X >= world.rodantX - 75 and world.trap0Y >= world.rodantY - 75:
            world.rodanthavelife = False
    if world.traplife1 == True: 
        if world.trap1X <= world.rodantX + 75 and world.trap1Y <= world.rodantY + 75 and world.trap1X >= world.rodantX - 75 and world.trap1Y >= world.rodantY -75:
            world.rodanthavelife = False
    if world.traplife2 ==True:    
        if world.trap2X <= world.rodantX + 75 and world.trap2Y <= world.rodantY + 75 and world.trap2X >= world.rodantX - 75 and world.trap2Y >= world.rodantY -75:
            world.rodanthavelife = False   
    if world.catX <= world.rodantX + 75 and world.catY <= world.rodantY + 75 and world.catX >= world.rodantX - 75 and world.catY >= world.rodantY -75:
        world.rodanthavelife = False 
     
                
    if isKeyPressed("return"):
        startWorld(world) 
            
    
###############################################
    #width = getImageWidth(world.rodant)
    #print width
    #height = getImageHeight(world.rodant)
    #print height
    #print (world.rodantX-width/2)-103
    #271
    #print (world.rodantX + width/2)+103
    #447
    #print (world.rodantY+height/2)+103
    #71
    #print (world.rodantY-height/2)-103
    #153
##################################################
    #Width =getImageWidth(world.trap0)
    #print Width
    #Height =getImageHeight(world.trap0)
    #print Height
#################################################
    #Width =getImageWidth(world.cheese0)
    #print Width
    #Height =getImageHeight(world.cheese0)
    #print Height
############################################################
#this function is called every frame to draw your worl

def drawWorld(world):  
    drawImage(world.lazers, world.lazersX, world.lazersY)
    if world.rodanthavecheese0 == False: 
        drawImage(world.cheese0, world.cheese0X, world.cheese0Y,)
    if world.rodanthavecheese1 == False:
        drawImage(world.cheese1, world.cheese1X, world.cheese1Y,)
    if world.rodanthavecheese2 == False:
        drawImage(world.cheese2, world.cheese2X, world.cheese2Y)
    if world.rodanthavecheese3 == False:
        drawImage(world.cheese3, world.cheese3X, world.cheese3Y,)
    if world.rodanthavecheese4 == False:
        drawImage(world.cheese4, world.cheese4X, world.cheese4Y,)
    if world.rodanthavecheese5 == False:
        drawImage(world.cheese5, world.cheese5X, world.cheese5Y,)
    if world.rodanthaveyarn==False:
        drawImage(world.yarn, world.yarnX, world.yarnY)
    if world.rodanthavelife == True:
        drawImage(world.rodant, world.rodantX, world.rodantY,)
    if world.rodanthavelife == False:
        drawImage(world.youlose, world.youloseX, world.youloseY) 
    if world.rodanthavecheese0==True and world.rodanthavecheese1==True and world.rodanthavecheese2==True and world.rodanthavecheese3==True and world.rodanthavecheese4==True and world.rodanthavecheese5==True and world.rodanthaveyarn== True:  
        drawImage(world.youwin, world.youwinX, world.youwinY)
    
    if world.traplife0 == True:
        drawImage(world.trap0, world.trap0X, world.trap0Y,)
    if world.traplife1 == True:  
        drawImage(world.trap1, world.trap1X, world.trap1Y,)
    if world.traplife2 == True:
        drawImage(world.trap2, world.trap2X, world.trap2Y,)
    drawImage(world.cat, world.catX, world.catY,)
   
    
    


############################################################
 
runGraphics(startWorld, updateWorld, drawWorld)
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 #sorces
 #http://www.clipartpanda.com/categories/cute-mouse-clipart